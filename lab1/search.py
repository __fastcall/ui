# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util
import copy

class SearchNode:
    """
    This class represents a node in the graph which represents the search problem.
    The class is used as a basic wrapper for search methods - you may use it, however
    you can solve the assignment without it.

    REMINDER: You need to fill in the backtrack function in this class!
    """

    def __init__(self, position, parent=None, transition=None, cost=0, heuristic=0):
        """
        Basic constructor which copies the values. Remember, you can access all the 
        values of a python object simply by referencing them - there is no need for 
        a getter method. 
        """
        self.position = position
        self.parent = parent
        self.cost = cost
        self.heuristic = heuristic
        self.transition = transition

    def isRootNode(self):
        """
        Check if the node has a parent.
        returns True in case it does, False otherwise
        """
        return self.parent == None 

    def unpack(self):
        """
        Return all relevant values for the current node.
        Returns position, parent node, cost, heuristic value
        """
        return self.position, self.parent, self.cost, self.heuristic


    def backtrack(self):
        """
        Reconstruct a path to the initial state from the current node.
        Bear in mind that usually you will reconstruct the path from the 
        final node to the initial.
        """
        moves = []
        # make a deep copy to stop any referencing isues.
        node = copy.deepcopy(self)

        if node.isRootNode(): 
            # The initial state is the final state
            return moves        

        moves = self.parent.backtrack()
        moves.append(self.transition)
        return moves


class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def __pushFuncDefault__(openedNodes, closedNodes, node, typePushFunc = (lambda openedNodes, node: openedNodes.push(node))):
    for closedNode in closedNodes:
        if node.position == closedNode.position:
            return
    typePushFunc(openedNodes, node)

def __pushFuncDefaultStack__(openedNodes, closedNodes, node):
    for openedNode in openedNodes.list:
        if node.position == openedNode.position:
            openedNodes.list.remove(openedNode) # optional, remove previous instance of same state for memory conservation
            break
    __pushFuncDefault__(openedNodes, closedNodes, node)

def __pushFuncDefaultQueue__(openedNodes, closedNodes, node):
    for openedNode in openedNodes.list:
        if node.position == openedNode.position:
            return # don't allow duplicates in the queue
    __pushFuncDefault__(openedNodes, closedNodes, node)

def __pushFuncDefaultPriorityQueue__(openedNodes, closedNodes, node):
    for openedNode in openedNodes.heap:
        if node.position == openedNode[2].position:
            if node.cost < openedNode[2].cost:
                openedNodes.heap.remove(openedNode) # remove previous instance of same state in favour of cheaper path alternative
                break
            else:
                return # ignore if cost is greater or equal
    __pushFuncDefault__(openedNodes, closedNodes, node, (lambda openedNodes, node: openedNodes.push(node, node.cost)))

def __genericSearch__(problem, listType = util.Stack, pushFunc = __pushFuncDefaultStack__, heuristicFunc = (lambda position, problem: 0)):

    # Opened and closed nodes
    openedNodes = listType()
    closedNodes = []

    # Push start node
    pushFunc(openedNodes, closedNodes, SearchNode(problem.getStartState()))

    # Search loop
    while not openedNodes.isEmpty():
        openedNode = openedNodes.pop()
        if problem.isGoalState(openedNode.position):
            return openedNode.backtrack()
        isAlreadyClosed = False
        for closedNode in closedNodes:
            if closedNode.position == openedNode.position:
                isAlreadyClosed = True
                break
        if not isAlreadyClosed:
            closedNodes.append(openedNode)
        for successor in problem.getSuccessors(openedNode.position):
            pushFunc(openedNodes, closedNodes, SearchNode(successor[0], openedNode, successor[1], openedNode.cost + successor[2], heuristicFunc(successor[0], problem)))

    # Return no path if none is found
    print('ERROR: No path found!')
    return []

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    """
    return __genericSearch__(problem)

def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    return __genericSearch__(problem, util.Queue, __pushFuncDefaultQueue__)

def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    return __genericSearch__(problem, util.PriorityQueue, __pushFuncDefaultPriorityQueue__)

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def __pushFuncAStar__(openedNodes, closedNodes, node):

    # Where similar node is found (if found)
    container = None
    similarMember = None
    similarNode = None

    # Try to find in opened nodes
    for openedNode in openedNodes.heap:
        if node.position == openedNode[2].position:
            container = openedNodes.heap
            similarMember = openedNode
            similarNode = openedNode[2]
            break

    # Try to find in closed nodes
    if similarNode == None:
        for closedNode in closedNodes:
            if node.position == closedNode.position:
                container = closedNodes
                similarMember = closedNode
                similarNode = closedNode
                break
    
    # If similar node was found
    if not similarNode == None:
        if similarNode.cost <= node.cost:
            return
        else:
            container.remove(similarMember)

    # Push similar node
    openedNodes.count = openedNodes.heap.__len__() # priority queue elements count doesn't refresh properly ?
    openedNodes.push(node, node.cost + node.heuristic)

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    return __genericSearch__(problem, util.PriorityQueue, __pushFuncAStar__, heuristic)


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
