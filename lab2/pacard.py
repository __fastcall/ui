
"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util
from logic import * 

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
        state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
        actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def miniWumpusSearch(problem): 
    """
    A sample pass through the miniWumpus layout. Your solution will not contain 
    just three steps! Optimality is not the concern here.
    """
    from game import Directions
    e = Directions.EAST 
    n = Directions.NORTH
    return  [e, n, n]

def logicBasedSearch(problem):
    """

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())

    print "Does the Wumpus's stench reach my spot?", 
               \ problem.isWumpusClose(problem.getStartState())

    print "Can I sense the chemicals from the pills?", 
               \ problem.isPoisonCapsuleClose(problem.getStartState())

    print "Can I see the glow from the teleporter?", 
               \ problem.isTeleporterClose(problem.getStartState())
    
    (the slash '\\' is used to combine commands spanning through multiple lines - 
    you should remove it if you convert the commands to a single line)
    
    Feel free to create and use as many helper functions as you want.

    A couple of hints: 
        * Use the getSuccessors method, not only when you are looking for states 
        you can transition into. In case you want to resolve if a poisoned pill is 
        at a certain state, it might be easy to check if you can sense the chemicals 
        on all cells surrounding the state. 
        * Memorize information, often and thoroughly. Dictionaries are your friends and 
        states (tuples) can be used as keys.
        * Keep track of the states you visit in order. You do NOT need to remember the
        tranisitions - simply pass the visited states to the 'reconstructPath' method 
        in the search problem. Check logicAgents.py and search.py for implementation.
    """
    # array in order to keep the ordering
    visitedStates = []
    startState = problem.getStartState()
    visitedStates.append(startState)
    
    # >>>>>>>>>>>> MY CODE <<<<<<<<<<<<<<

    # build knowledgebase
    (width, height) = (problem.walls.width - 2, problem.walls.height - 2)
    kb = dict()
    for x in range(1, width + 1):
        for y in range(1, height + 1):
            kb[(x, y)] = StateKnowledge()

    # discovered state sets
    safeStates = [startState]
    unsafeStates = []
    unknownStates = []
    for x in range(1, width + 1):
        for y in range(1, height + 1):
            if (x, y) != startState:
                unknownStates.append((x, y))
    
    # current state
    currentState = startState
    kb[currentState].isWumpus = False
    kb[currentState].isPoison = False
    kb[currentState].isTeleporter = False
    kb[currentState].isSafe = True

    # solve for each state
    while True:

        # print visiting message
        print("Visiting: " + str(currentState))

        # if teleporter on field, complete game
        if problem.isGoalState(currentState):
            break
        # if wumpus/poison on field, die (safety net!)
        elif problem.isWumpus(currentState) or problem.isPoisonCapsule(currentState):
            raise Exception("Game over: You died!")

        # set state-specific sensory response
        kb[currentState].isWumpusClose = problem.isWumpusClose(currentState)
        kb[currentState].isPoisonClose = problem.isPoisonCapsuleClose(currentState)
        kb[currentState].isTeleporterClose = problem.isTeleporterClose(currentState)

        # found teleporter
        foundTeleporter = False

        # lists for prepend
        safeList = []
        unsafeList = []

        # process each movement successor
        for move in problem.getSuccessors(currentState):

            # extract movement position (state)
            moveState = move[0]

            # ignore if state was already visited
            if moveState in visitedStates:
                continue

            # resolve state position
            resolveState(currentState, moveState, kb)

            # if state is teleporter, move immediately
            if (kb[moveState].isTeleporter):
                currentState = moveState
                visitedStates.append(currentState)
                foundTeleporter = True
                break

            # remove from all state sets
            if moveState in safeStates:
                safeStates.remove(moveState)
            if moveState in unsafeStates:
                unsafeStates.remove(moveState)

            # add state to appropriate group
            if kb[moveState].isSafe is not None:
                if kb[moveState].isSafe == True:
                    safeList.append(moveState)
                else:
                    unsafeList.append(moveState)

        # break if teleporter was found
        if foundTeleporter:
            break

        # merge lists
        safeList.extend(safeStates)
        safeStates = safeList
        unsafeList.extend(unsafeStates)
        unsafeStates = unsafeList

        # try to find next state
        while True:
            if len(safeStates) > 0:
                currentState = safeStates.pop(0)
                if currentState in visitedStates:
                    continue
                visitedStates.append(currentState)
                break
            else:
                raise Exception("Game over: No more safe states!")

    # print success message and reconstruct path
    print("Game over: Teleported home!")
    return problem.reconstructPath(visitedStates)

# represents knowledge about one field
class StateKnowledge:
    def __init__(self):
        self.isWumpus = None
        self.isWumpusClose = None
        self.isPoison = None
        self.isPoisonClose = None
        self.isTeleporter = None
        self.isTeleporterClose = None
        self.isSafe = None

# resolves state with logic
def resolveState(currState, moveState, kb):

    # construct ruleset
    ruleset = constructRulesetTrans(currState, moveState, kb)

    # movement class
    m = kb[moveState]

    # resolve is wumpus, poison and teleporter
    m.isWumpus = resolveEntity(moveState, ruleset, Labels.WUMPUS, m.isWumpus)
    m.isPoison = resolveEntity(moveState, ruleset, Labels.POISON, m.isPoison)
    m.isSafe = resolveEntity(moveState, ruleset, Labels.SAFE, m.isSafe)

# resolves entity ruleset conclusions
def resolveEntity(state, ruleset, entitylbl, initial):
    if initial is None:
        yr = Clause([Literal(entitylbl, state, False)])
        if resolution(ruleset, yr):
            initial = True
            ruleset.add(yr)
        else:
            nr = Clause([Literal(entitylbl, state, True)])
            if resolution(ruleset, nr):
                initial = False
    return initial

# constructs ruleset for transition
def constructRulesetTrans(currState, moveState, kb):
    ruleset = constructRuleset(currState, kb)
    ruleset.update(constructKnowledge(moveState, kb))
    return ruleset

# constructs ruleset for one field and its neighbours
def constructRuleset(state, kb):

    # unpack state
    (x, y) = state

    # construct list of states
    states = set({state})
    if (x - 1, y) in kb:
        states.add((x - 1, y))
    if (x + 1, y) in kb:
        states.add((x + 1, y))
    if (x, y - 1) in kb:
        states.add((x, y - 1))
    if (x, y + 1) in kb:
        states.add((x, y + 1))

    # construct rules for all states
    ruleset = set()
    for s in states:
        ruleset.update(constructKnowledge(s, kb))

    # return ruleset
    return ruleset

# construct knowledge within one state
def constructKnowledge(state, kb):
    k = set()
    s = kb[state]
    if (s.isWumpus is not None):
        k.add(Clause([Literal(Labels.WUMPUS, state, not s.isWumpus)]))
    if (s.isWumpusClose is not None):
        k.add(Clause([Literal(Labels.WUMPUS_STENCH, state, not s.isWumpusClose)]))
    if (s.isPoison is not None):
        k.add(Clause([Literal(Labels.POISON, state, not s.isPoison)]))
    if (s.isPoisonClose is not None):
        k.add(Clause([Literal(Labels.POISON_FUMES, state, not s.isPoisonClose)]))
    if (s.isTeleporter is not None):
        k.add(Clause([Literal(Labels.TELEPORTER, state, not s.isTeleporter)]))
    if (s.isTeleporterClose is not None):
        k.add(Clause([Literal(Labels.TELEPORTER_GLOW, state, not s.isTeleporterClose)]))
    if (s.isSafe is not None):
        k.add(Clause([Literal(Labels.SAFE, state, not s.isSafe)]))
    k.update(genSensingRule(state, Labels.WUMPUS_STENCH, Labels.WUMPUS))
    k.update(genNonSensingRule(state, Labels.WUMPUS_STENCH, Labels.WUMPUS))
    k.update(genSensingRule(state, Labels.POISON_FUMES, Labels.POISON))
    k.update(genNonSensingRule(state, Labels.POISON_FUMES, Labels.POISON))
    k.update(genSensingRule(state, Labels.TELEPORTER_GLOW, Labels.TELEPORTER))
    k.update(genNonSensingRule(state, Labels.TELEPORTER_GLOW, Labels.TELEPORTER))
    k.update(genSafeNeighboursRule(state))
    k.update(genSafeFieldRule(state))
    return k

# generate sensing rules
def genSensingRule(state, senselbl, entitylbl):
    (x, y) = state
    rules = set({Clause([
        Literal(senselbl, (x, y), True),
        Literal(entitylbl, (x - 1, y), False),
        Literal(entitylbl, (x + 1, y), False),
        Literal(entitylbl, (x, y - 1), False),
        Literal(entitylbl, (x, y + 1), False)
    ])})
    return rules

# generate non sensing rules
def genNonSensingRule(state, senselbl, entitylbl):
    (x, y) = state
    rules = set({
        Clause([
            Literal(senselbl, (x, y), False),
            Literal(entitylbl, (x - 1, y), True)
        ]),
        Clause([
            Literal(senselbl, (x, y), False),
            Literal(entitylbl, (x + 1, y), True)
        ]),
        Clause([
            Literal(senselbl, (x, y), False),
            Literal(entitylbl, (x, y - 1), True)
        ]),
        Clause([
            Literal(senselbl, (x, y), False),
            Literal(entitylbl, (x, y + 1), True)
        ])
    })
    return rules

# generate safe neighbouring fields rules
def genSafeNeighboursRule(state):
    (x, y) = state
    rules = set({
        Clause([
            Literal(Labels.WUMPUS_STENCH, (x, y), False),
            Literal(Labels.POISON_FUMES, (x, y), False),
            Literal(Labels.SAFE, (x - 1, y), False)
        ]),
        Clause([
            Literal(Labels.WUMPUS_STENCH, (x, y), False),
            Literal(Labels.POISON_FUMES, (x, y), False),
            Literal(Labels.SAFE, (x + 1, y), False)
        ]),
        Clause([
            Literal(Labels.WUMPUS_STENCH, (x, y), False),
            Literal(Labels.POISON_FUMES, (x, y), False),
            Literal(Labels.SAFE, (x, y - 1), False)
        ]),
        Clause([
            Literal(Labels.WUMPUS_STENCH, (x, y), False),
            Literal(Labels.POISON_FUMES, (x, y), False),
            Literal(Labels.SAFE, (x, y + 1), False)
        ])
    })
    return rules

# generate safe field rules
def genSafeFieldRule(state):
    (x, y) = state
    results = set({Clause([
        Literal(Labels.WUMPUS, (x, y), False),
        Literal(Labels.POISON, (x, y), False),
        Literal(Labels.SAFE, (x, y), False)
    ])})
    return results

# Abbreviations
lbs = logicBasedSearch
