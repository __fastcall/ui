from logic import Clause, Literal, resolution

def parseLiteral(s):
    n = False
    if s[0] == '~':
        n = True
        s = s[1:]
    return Literal(s, (0, 0), n)

def test1():
    premises = [
        Clause(map(parseLiteral, 'A,~B,C,F,G,H'.split(','))),
        Clause(map(parseLiteral, 'B,~C,~F,~D'.split(','))),
        Clause(map(parseLiteral, 'A,X,~C'.split(','))),
        Clause(map(parseLiteral, '~A,~B,~C,F,~G'.split(','))),
        Clause(map(parseLiteral, '~G,~H'.split(','))),
        Clause(map(parseLiteral, '~B'.split(','))),
        Clause(map(parseLiteral, '~H'.split(','))),
        Clause(map(parseLiteral, '~D'.split(',')))
    ]
    goalTrue = Clause(map(parseLiteral, '~C,~D'.split(',')))
    goalFalse = Clause(map(parseLiteral, 'H'.split(',')))
    assert(resolution(premises, goalTrue) == True)
    assert(resolution(premises, goalFalse) == False)

def test2():
    premises = [
        Clause(map(parseLiteral, 'A,B,C'.split(','))),
        Clause(map(parseLiteral, '~A,B,C'.split(','))),
        Clause(map(parseLiteral, '~A,B,C,D'.split(',')))
    ]
    goalTrue = Clause(map(parseLiteral, 'B,C'.split(',')))
    goalFalse = Clause(map(parseLiteral, '~B,C'.split(',')))
    assert(resolution(premises, goalTrue) == True)
    assert(resolution(premises, goalFalse) == False)

def test3():
    premises = [
        Clause(map(parseLiteral, 'A,B,~C'.split(','))),
        Clause(map(parseLiteral, '~A,~B,C'.split(',')))
    ]
    goalFalse1 = Clause(map(parseLiteral, 'A'.split(',')))
    goalFalse2 = Clause(map(parseLiteral, '~A'.split(',')))
    assert(resolution(premises, goalFalse1) == False)
    assert(resolution(premises, goalFalse2) == False)

def test4():
    premises = [
        Clause(map(parseLiteral, 'A,~B,C,~D,E,~F'.split(','))),
        Clause(map(parseLiteral, '~A,B,~C,D,E,F'.split(','))),
        Clause(map(parseLiteral, 'A,~B,C,D,~D,F'.split(','))),
        Clause(map(parseLiteral, 'A,B,~C'.split(','))),
        Clause(map(parseLiteral, '~A,~B,D'.split(','))),
        Clause(map(parseLiteral, '~A,B,E'.split(',')))
    ]
    goalTrue = Clause(map(parseLiteral, '~B,C,D,F'.split(',')))
    goalFalse = Clause(map(parseLiteral, '~E'.split(',')))
    assert(resolution(premises, goalTrue) == True)
    assert(resolution(premises, goalFalse) == False)

def test5():
    premises = [
        Clause(map(parseLiteral, '~A,B,C,~D'.split(','))),
        Clause(map(parseLiteral, 'A,~B,~C,D'.split(','))),
        Clause(map(parseLiteral, 'A,~B,C,D'.split(','))),
        Clause(map(parseLiteral, '~A,B,C,~D'.split(','))),
        Clause(map(parseLiteral, 'A,~B,C,D'.split(',')))
    ]
    goalTrue = Clause(map(parseLiteral, 'A,~B,D'.split(',')))
    goalFalse = Clause(map(parseLiteral, '~C'.split(',')))
    assert(resolution(premises, goalTrue) == True)
    assert(resolution(premises, goalFalse) == False)

def test6():
    premises = [
        Clause(map(parseLiteral, '~A,B,C'.split(','))),
        Clause(map(parseLiteral, 'A,B,~C'.split(','))),
        Clause(map(parseLiteral, '~A,~B,C'.split(','))),
    ]
    goalTrue = Clause(map(parseLiteral, '~A,C'.split(',')))
    goalFalse = Clause(map(parseLiteral, '~B'.split(',')))
    assert(resolution(premises, goalTrue) == True)
    assert(resolution(premises, goalFalse) == False)

if __name__ == '__main__':
    test1()
    test2()
    test3()
    test4()
    test5()
    test6()
