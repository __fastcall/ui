from logic import Labels, Literal, Clause

def _genSensingRules(width, height, senselbl, entitylbl):
    results = set()
    for x in range(1, width + 1):
        for y in range(1, height + 1):
            results.add(Clause([
                Literal(senselbl, (x, y), True),
                Literal(entitylbl, (x - 1, y), False),
                Literal(entitylbl, (x + 1, y), False),
                Literal(entitylbl, (x, y - 1), False),
                Literal(entitylbl, (x, y + 1), False)
            ]))
    return results

def _genNonSensingRules(width, height, senselbl, entitylbl):
    results = set()
    for x in range(1, width + 1):
        for y in range(1, height + 1):
            results.update({
                Clause([
                    Literal(senselbl, (x, y), False),
                    Literal(entitylbl, (x - 1, y), True)
                ]),
                Clause([
                    Literal(senselbl, (x, y), False),
                    Literal(entitylbl, (x + 1, y), True)
                ]),
                Clause([
                    Literal(senselbl, (x, y), False),
                    Literal(entitylbl, (x, y - 1), True)
                ]),
                Clause([
                    Literal(senselbl, (x, y), False),
                    Literal(entitylbl, (x, y + 1), True)
                ])
            })
    return results

def genSenseStenchRules(width, height):
    return _genSensingRules(width, height, Labels.WUMPUS_STENCH, Labels.WUMPUS)

def genNonSensingStenchRules(width, height):
    return _genNonSensingRules(width, height, Labels.WUMPUS_STENCH, Labels.WUMPUS)

def genSenseFumesRules(width, height):
    return _genSensingRules(width, height, Labels.POISON_FUMES, Labels.POISON)

def genNonSensingFumesRules(width, height):
    return _genNonSensingRules(width, height, Labels.POISON_FUMES, Labels.POISON)

def genSenseGlowRules(width, height):
    return _genSensingRules(width, height, Labels.TELEPORTER_GLOW, Labels.TELEPORTER)

def genNonSensingGlowRules(width, height):
    return _genNonSensingRules(width, height, Labels.TELEPORTER_GLOW, Labels.TELEPORTER)

def _genWumpusSinglePositionRules(width, height, posx, posy):
    results = set()
    for x in range(1, width + 1):
        for y in range(1, height + 1):
            if (x, y) != (posx, posy):
                results.add(Clause([
                    Literal(Labels.WUMPUS, (posx, posy), True),
                    Literal(Labels.WUMPUS, (x, y), True)
                ]))
    return results

def genWumpusPositionRules(width, height):
    results = set()
    for x in range(1, width + 1):
        for y in range(1, height + 1):
            results.update(_genWumpusSinglePositionRules(width, height, x, y))
    return results

def genSafeNeighbouringFieldRules(width, height):
    results = set()
    for x in range(1, width + 1):
        for y in range(1, height + 1):
            results.update({
                Clause([
                    Literal(Labels.WUMPUS_STENCH, (x, y), False),
                    Literal(Labels.POISON_FUMES, (x, y), False),
                    Literal(Labels.SAFE, (x - 1, y), False)
                ]),
                Clause([
                    Literal(Labels.WUMPUS_STENCH, (x, y), False),
                    Literal(Labels.POISON_FUMES, (x, y), False),
                    Literal(Labels.SAFE, (x + 1, y), False)
                ]),
                Clause([
                    Literal(Labels.WUMPUS_STENCH, (x, y), False),
                    Literal(Labels.POISON_FUMES, (x, y), False),
                    Literal(Labels.SAFE, (x, y - 1), False)
                ]),
                Clause([
                    Literal(Labels.WUMPUS_STENCH, (x, y), False),
                    Literal(Labels.POISON_FUMES, (x, y), False),
                    Literal(Labels.SAFE, (x, y + 1), False)
                ])
            })
    return results

def genSafeFieldRules(width, height):
    results = set()
    for x in range(1, width + 1):
        for y in range(1, height + 1):
            results.add(Clause([
                Literal(Labels.WUMPUS, (x, y), False),
                Literal(Labels.POISON, (x, y), False),
                Literal(Labels.SAFE, (x, y), False)
            ]))
    return results

def genRules(width, height):
    results = genSenseStenchRules(width, height)
    results.update(genNonSensingStenchRules(width, height))
    results.update(genSenseFumesRules(width, height))
    results.update(genNonSensingFumesRules(width, height))
    results.update(genSenseGlowRules(width, height))
    results.update(genNonSensingGlowRules(width, height))
    # results.update(genWumpusPositionRules(width, height))
    results.update(genSafeNeighbouringFieldRules(width, height))
    results.update(genSafeFieldRules(width, height))
    return results
